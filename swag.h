#ifndef SWAG_H
# define SWAG_H

# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>

void	finish(char **map, int *sizpcn);
void	rem(char **map, char **pcs, int *ofsxy, int **coords);
int		place(char **map, char **pcs, int *ofsxy, int **coords);
void	beq(char **map, char **pcs, int *sizpcn);

#endif