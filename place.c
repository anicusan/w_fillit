#include "swag.h"

void	finish(char **map, int *sizpcn)
{
	int		i;

	i = 0;
	while (i < sizpcn[0])
	{
		write(1, map[i], sizpcn[0] + 1);
		//write(1, '\n', 1);
		i++;
	}
	sizpcn[1] = - 42;
}

void	rem(char **map, char **pcs, int *ofsxy, int **coords)
{
	int     xmap;
	int     ymap;
	int     xpcs;
	int     ypcs;

	xmap = ofsxy[0];
	xpcs = coords[0][0];
	while (xpcs <= coords[1][0])
	{
		ypcs = coords[0][1];
		ymap = ofsxy[1];
		while (ypcs <= coords[1][1])
		{
			if (pcs[xpcs][ypcs] == '#')
				map[xmap][ymap] = '.';
			ypcs++;
			ymap++;
		}
		xpcs++;
		xmap++;
	}
}

int     defplace(char **map, char **pcs, int *ofsxy, int **coords)
{
	int     xmap;
	int     ymap;
	int     xpcs;
	int     ypcs;

	xmap = ofsxy[0];
	xpcs = coords[0][0];
	while (xpcs <= coords[1][0])
	{
		ypcs = coords[0][1];
		ymap = ofsxy[1];
		while (ypcs <= coords[1][1])
		{
			if (pcs[xpcs][ypcs] == '#')
				map[xmap][ymap] = 'A' + ofsxy[2] - 1;
			ypcs++;
			ymap++;
		}
		xpcs++;
		xmap++;
	}
	return (1);
}

int     place(char **map, char **pcs, int *ofsxy, int **coords)
{
	int     xmap;
	int     ymap;
	int     xpcs;
	int     ypcs;

	xmap = ofsxy[0];
	xpcs = coords[0][0];
	while (xpcs <= coords[1][0])
	{
		ypcs = coords[0][1];
		ymap = ofsxy[1];
		while (ypcs <= coords[1][1])
		{
			if (map[xmap][ymap] != '.' && pcs[xpcs][ypcs] == '#')
				return (0);
			ypcs++;
			ymap++;
		}
		xpcs++;
		xmap++;
	}
	return (defplace(map, pcs, ofsxy, coords));
}