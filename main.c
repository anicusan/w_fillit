#include "swag.h"
#include <stdio.h>
#include <string.h>

char    **genmap(int n)
{
    int     i;
    char    **map;

    i = 0;
    map = (char**)malloc(sizeof(char*) * n);
    while (i < n)
    {
        map[i] = (char*)malloc(sizeof(char) * n + 2);
        map[i] = strncpy(map[i], "....................................", n);
        map[i][n] = '\n';
        map[i][n + 1] = '\0';
        i++;
    }
    return (map);
}

char    **getpcs(int fd)
{
    char    **pcs;
    int     i;

    i = 0;
    pcs = (char**)malloc(sizeof(char*) * 40);
    while (i < 40)
    {
        pcs[i] = (char*)malloc(sizeof(char) * 5);
        if ( (i + 1) % 5)
            read(fd, pcs[i++], 5);
        else
        {
            read(fd, pcs[i], 1);
            //pcs[i][1] = '\0';
            i++;
        }
    }
    return (pcs);
}

int     pathfd(char *s)
{
    return (open(s, O_RDONLY));
}

int     main(int av, char **ac)
{
    char    **pcs;
    char    **map;
    int     *sizpcn;
    int     i;

    i = 6;
    sizpcn = (int*)malloc(sizeof(int) * 4);
    sizpcn[2] = 8;
    if (av == 2)
    {
        pcs = getpcs(pathfd(ac[1]));
        sizpcn[1] = 1;
        while (sizpcn[1] > 0)
        {
            sizpcn[1] = 1;
            map = genmap(i);
            sizpcn[0] = i;
            beq(map, pcs, sizpcn);
            //printf("\n%d %d %d\n", sizpcn[0], sizpcn[1], sizpcn[2]);
            i++;
        }
    }
    return (0);
}